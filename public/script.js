var lastMeasured = null;
var shouldPopulateCharts = false;
var sensors = [];

displayDate();

const station = getStationID();

if (station == null) {
  alert("Station ID is missing!");
} else {
  // link station name to OpenSenseMap Page
  document.getElementById("osm-link").href += station;

  const apiBase = "https://api.opensensemap.org/boxes/" + station;
  const sensorBase = apiBase + "/sensors";

  getStationDetails(apiBase).then(station_details => {
    // set page title
    document.title = station_details.name + " | " + "senseboard" ;

    // set station name
    stationTitle = document.getElementById("station-name");
    stationTitle.innerText = station_details.name;

    // show station description
    if (station_details.hasOwnProperty("description")) {
      stationDesc = document.getElementById("station-desc");
      stationDesc.innerText = station_details.description;
    }

    // update last measured time
    lastMeasured = new Date(station_details.lastMeasurementAt);
    lastUpdated = document.getElementById("last-updated");
    lastUpdated.innerText = lastMeasured.toLocaleString();

    // update relative time
    // updateRelativeTime(lastMeasured);
    
    // populate sensors
    sensorSection = document.getElementById("sensors");
    for (sensor of station_details.sensors) {
      sensors.push(sensor);
      sensorElement = addSensor(sensor, station_details.sensors.length);
      sensorSection.appendChild(sensorElement);
    }

    // create charts
    createCharts(sensors);
    
    // populate value in chart
    populateCharts(apiBase, sensors);

    // periodically update values
    //periodicUpdates(apiBase);
    setInterval(periodicUpdates, 60*1000, apiBase);
  });
}

function displayDate() {
  const date = new Date();
  dateSection = document.getElementById("date-section");
  dateSection.innerText = date.toDateString() + " " + date.toLocaleTimeString();
  if (lastMeasured)
    updateRelativeTime(lastMeasured)
  setTimeout(displayDate, 1000);
};

function getStationID() {
  var urlParams = new URLSearchParams(window.location.search);
  return urlParams.has("station") ? urlParams.get("station") : null;
}

async function getStationDetails(url) {
  resp = await fetch(url);
  if (resp.status == 200) {
    return await resp.json();
  }
  return null;
}

function addSensor(sensor, totalSensors) {
  sensorDiv = document.createElement("div");
  sensorDiv.classList.add("pure-u-1");
  sensorDiv.classList.add("pure-u-1-" + totalSensors);
  sensorDiv.classList.add("pure-u-md-1-" + totalSensors);
  sensorDiv.id = sensor.title;

  sensorName = document.createElement("p");
  sensorName.id = "sensorName"
  sensorName.classList.add("sensorName");
  sensorName.innerText = sensor.title;
  sensorDiv.appendChild(sensorName);

  sensorValue = document.createElement("p");
  sensorValue.id = "sensorValue"
  sensorValue.classList.add("sensorValue");
  if (sensor.lastMeasurement == null)
    sensorValue.innerText = "x"
  else
    sensorValue.innerText = sensor.lastMeasurement.value + " " + sensor.unit;
  sensorDiv.appendChild(sensorValue);

  return sensorDiv;
}

function updateRelativeTime(lastMeasured) {
  relativeSection = document.getElementById("last-updated-relative");
  if (relativeSection != null)
    relativeSection.innerText = "(" + moment(lastMeasured).fromNow() + ")";
}

function getChartDataSet() {
  datasets = []
  for (sensor of sensors) {
    datasets.push({
      data: [],
      label: sensor.title,
      borderColor: "red"
    })
  }
  return datasets;
}

function createCharts(sensors) {
  const colors = ["#ff6384", "#36a2eb", "#9966ff", "#4bc0c0", "#ff9f40", "#ffcd56"];
  const chartRowsNeeded = Math.ceil(sensors.length/2);  // Two charts in a row.
  chartSection = document.getElementById("charts");

  let sensorAdded = 0;
  for (let row=1; row < (chartRowsNeeded+1); row++) {
      rowDiv = document.createElement("div");
      rowDiv.classList.add("pure-g");

      sensorsToAdd = sensors.slice(sensorAdded, (sensorAdded+2));
      for (let sensor of sensorsToAdd) {
        chartDiv = document.createElement("div");
        chartDiv.classList.add("pure-u-1", "pure-u-1-2", "pure-u-md-1-2");
        chartCanvas = addChart(sensor, colors[sensorAdded]);
        chartDiv.appendChild(chartCanvas);
        rowDiv.appendChild(chartDiv);
        sensorAdded += 1;
      }
      chartSection.appendChild(rowDiv);
  }
}

function addChart(sensor, color) {
  canvas_element = document.createElement("canvas");
  canvas_element.id = sensor._id;

  new Chart(canvas_element, {
    type: 'line',
    data: {
      labels: [],
      datasets: [
        {
          data: [],
          label: sensor.title + " in " + sensor.unit,
          borderColor: color,
        }
      ],
    },
    options: {
      responsive: true,
      plugins: {
        zoom: {
          pan: {
            enabled: true,
            mode: 'x',
            scaleMode: 'y'
          },
          zoom: {
            mode: 'x',
            scaleMode: 'y',
            pinch: {
              enabled: true,
            },
            wheel: {
              enabled: true,
            },
            drag: {
              enabled: true
            }
          },
        }
      }
    }
  });
  return canvas_element;
};

function populateCharts(baseUrl, sensors) {
  let now = new Date();
  let past = moment(now).subtract(2, 'days');
  let scales = {
    x: {
      type: "time",
      time: {
        displayFormats: {
          hour: "HH:mm",
          minute: "HH:mm",
        }
      },
      distribution: "linear"
    }
  };
  
  let plugins = {
    title: {
      display: true,
      text: past.format('DD/MM/YYYY') + " - " + now.toLocaleDateString()
    },
    colors: {
      forceOverride: false
    },
    zoom: {
      pan: {
        enabled: true,
        mode: "x",
        scaleMode: 'y'
      },
      zoom: {
        wheel: {
          enabled: true
        },
        pinch: {
          enabled: true
        },
        mode: "x",
        scaleMode: 'y'
      }
    }
  };
  
  for (sensor of sensors) {
    let chart = Chart.getChart(sensor._id);
    let url = new URL(baseUrl + "/data/" + sensor._id);
    let params = url.searchParams;
    params.append("from-date", past.toISOString());
    params.append("to-date", now.toISOString());
    
    getSensorData(url).then(json_data => {        
      let data = [];
      for (let entry of json_data) {
        data.push({
          x: entry.createdAt,
          y: entry.value
        });
      }

      chart.data.datasets[0].data = data.reverse();
      chart.options.scales = scales;
      chart.options.plugins = plugins;
      chart.update();
    });
  }
}

async function getSensorData(url) {
  let resp = await fetch(url);
  if (resp.status == 200)
    return resp.json();
  return null;
}

function periodicUpdates(url) {
  getStationDetails(url).then(station => {
    recentlyMeasured = new Date(station.lastMeasurementAt);
    if (recentlyMeasured > lastMeasured) {
      lastMeasured = recentlyMeasured;

      lastUpdated = document.getElementById("last-updated");
      lastUpdated.innerText = lastMeasured.toLocaleString();

      for (sensor of station.sensors) {
        // update sensor display value
        sensorDiv = document.getElementById(sensor.title);
        if (sensorDiv)
          sensorDiv.children.sensorValue.innerText = sensor.lastMeasurement.value + " " + sensor.unit;

        // update chart with new value
        chartCanvas = Chart.getChart(sensor._id);
        if (chartCanvas) {
          data = {
            x: sensor.lastMeasurement.createdAt,
            y: sensor.lastMeasurement.value,
          }
          chartCanvas.data.datasets[0].data.push(data);
          chartCanvas.update();
        }   
      }
    }
  });
}
